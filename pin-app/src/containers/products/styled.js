import styled from 'styled-components'
import mediaQueries from '../../utils/media-queries'

export const CenterWrapper = styled.div`
    padding: 10px 25%;
    display:flex;
    align-items: center;
    justify-content: center;
`
export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 40px 80px;
    ${mediaQueries.mobile`
      padding: 15px 30px;
    `};
`
export const StripWrapper = styled.div`
    width:70%;
    padding:5px;
    ${mediaQueries.tablet`
      width:90%;
    `};
    ${mediaQueries.mobile`
      width:100%;
    `};
`