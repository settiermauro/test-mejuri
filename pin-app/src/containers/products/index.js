import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {addProductRender, fetchProducts, likeProduct, unlikeProduct} from '../../actions/products'
import {
    getProducts,
    getProductsError,
    getProductsLiked,
    getProductsPending,
    getProductsRender,
    getProductsType
} from '../../selectors/products'
import Loader from 'react-loader-spinner'
import theme from '../../theme'
import ListGrid from '../../atoms/list-grid'
import UnexpectedError from '../../molecules/errors/unexpected'
import {CenterWrapper, StripWrapper, Wrapper} from './styled'
import ProductPreview from '../../organisms/product-preview'
import ProductsTotal from '../../organisms/products-total'
import ButtonStrip from '../../molecules/button-strip'
import language from '../../language'
import constants from '../../utils/constans'
import debounce from 'lodash.debounce'

class Products extends Component{

    state = {
        filter_buttons: [
            {label: language.types[constants.likes], value: constants.likes},
            {label: language.types[constants.type.rings], value: constants.type.rings},
            {label: language.types[constants.type.pendants], value: constants.type.pendants},
            {label: language.types[constants.type.earrings], value: constants.type.earrings},
            {label: language.types[constants.type.bracelets], value: constants.type.bracelets}
        ],
        type_selected: constants.type.rings
    }

    constructor(props) {
        super(props)
        // Binds our scroll event handler
        window.onscroll = debounce(() => {
            const { pending, error, products_render, products, addProductRender } = this.props

            // Bails early if:
            // * there's an error
            // * it's already loading
            // * there's nothing left to load
            if (error || pending || products_render.length === products.length) return

            // Checks that the page has scrolled to the bottom
            if (
                window.innerHeight + document.documentElement.scrollTop
                === document.documentElement.offsetHeight
            ) {
                addProductRender()
            }
        }, 100)
    }

    componentWillMount() {
        const {type_selected} = this.state
        const {fetchProducts} = this.props
        fetchProducts(type_selected)
    }

    onFilterButtonClick = (value) => {
        const {type_selected} = this.state
        const new_type = value === type_selected ? null : value
        this.setState({
            type_selected: new_type
        })
    }

    componentDidUpdate(prevProps, prevState) {
        const {type_selected} = this.state
        const {fetchProducts} = this.props
        if(prevState.type_selected !== type_selected && type_selected !== constants.likes) {
            fetchProducts(type_selected)
        }
    }

    openMejuriPageClick = () => window.open('https://mejuri.com')

    onSaveProductClick = (isSaved, product) => isSaved ? this.props.unlikeProduct(product) : this.props.likeProduct(product)

    render() {
        const { filter_buttons, type_selected } = this.state
        const {pending, error, products_render, products_liked, products} = this.props
        const products_to_render = type_selected === constants.likes ? products_liked : products_render
        const total = type_selected === constants.likes ? products_liked.length : products.length

        if(error){
            return (
                <CenterWrapper>
                    <UnexpectedError />
                </CenterWrapper>
            )
        }

        return (
            <Wrapper>
                <StripWrapper>
                    <ButtonStrip
                        disabled={pending}
                        items={filter_buttons}
                        selected={type_selected}
                        onClick={(value) => this.onFilterButtonClick(value)} />
                </StripWrapper>
                {pending ?
                    <CenterWrapper>
                        <Loader
                            type={'Grid'}
                            color={theme.colors.blue}/>
                    </CenterWrapper>
                    :
                    <Fragment>
                        <ProductsTotal total={total}/>
                        <ListGrid items={products_to_render.map(product =>
                            <ProductPreview
                                isSaved={products_liked.find(like => like.id === product.id)}
                                onSaveClick={(isSaved) => this.onSaveProductClick(isSaved, product)}
                                onMejuriClick={this.openMejuriPageClick}
                                src={product.variant_images[0].attachment_url_original}/>)} />
                    </Fragment>
                }
            </Wrapper>
        )
    }
}


const mapStateToProps = state => ({
    error: getProductsError(state),
    pending: getProductsPending(state),
    products: getProducts(state),
    products_render: getProductsRender(state),
    products_liked: getProductsLiked(state),
    type_selected: getProductsType(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchProducts,
    likeProduct,
    unlikeProduct,
    addProductRender
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Products)