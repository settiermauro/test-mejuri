import constants from '../utils/constans'

export default {
    labels:{
        save: 'Save',
        remove: 'Remove',
        mejuri: 'mejuri',
        product_name: 'Pins'
    },
    types:{
        [constants.likes]: 'Likes',
        [constants.type.rings]: 'Rings',
        [constants.type.earrings]: 'Earrings',
        [constants.type.pendants]: 'Necklaces',
        [constants.type.bracelets]: 'Bracelets + Anklets'
    },
    errors:{
        unexpected:{
            title: 'Oops, we have a problem.',
            description: 'Something went wrong, please try again later.'
        }
    }
}