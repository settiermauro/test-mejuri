export default {
    type:{
        rings: 'rings',
        pendants: 'pendants',
        earrings: 'earrings',
        bracelets: 'bracelets'
    },
    likes: 'likes',
    render_qty: 30
}