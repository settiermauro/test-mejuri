import {FETCH_PRODUCTS_PENDING, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR, LIKE_PRODUCT, UNLIKE_PRODUCT, ADD_PRODUCT_RENDER} from '../actions/products'
import initial_state from './initial_state'
import constants from '../utils/constans'

const default_state = initial_state.products

export const products = (state = default_state, action) => {
    switch(action.type) {
        case FETCH_PRODUCTS_PENDING:
            return {
                ...state,
                pending: true,
                products: []
            }
        case FETCH_PRODUCTS_SUCCESS:
            const productsCategoryArray = action.products
            let action_products = []
            productsCategoryArray.forEach(productCategory => {
                productCategory.products.forEach(product => action_products.push(product))
            })
            return {
                ...state,
                pending: false,
                products: action_products,
                products_render: action_products.slice(0, constants.render_qty)
            }
        case FETCH_PRODUCTS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case LIKE_PRODUCT:
            let new_likes = state.likes
            if(!new_likes.find(like => like.id === action.product.id)){
                new_likes = new_likes.concat(action.product)
            }
            return {
                ...state,
                likes: new_likes
            }
        case UNLIKE_PRODUCT:
            return {
                ...state,
                likes: state.likes.filter(like => like.id !== action.product.id)
            }
        case ADD_PRODUCT_RENDER:
            const { products_render, products } = state
            const begin = products_render.length
            const end = begin + constants.render_qty
            return {
                ...state,
                products_render: products_render.concat(products.slice(begin, end))
            }
        default:
            return state
    }
}