const theme = {
  colors: {
    white: '#ffffff',
    black: '#000000',
    gray_light: '#f5f5f5',
    gray: '#bdbdbd',
    gray_dark: '#5c5c5c',
    blue: '#c9dff5',
    red: '#d0021b'
  },
  shadow: {
    black10: 'rgba(0, 0, 0, 0.10)',
    black25: 'rgba(0, 0, 0, 0.25)'
  }
}

export default theme
