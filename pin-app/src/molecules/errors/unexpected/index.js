import React from 'react'
import {Container, Description, Title} from './styled'
import Image from '../../../atoms/image'
import language from '../../../language'

const UnexpectedError = () =>
    (
        <Container>
            <Title>
                {language.errors.unexpected.title}
            </Title>
            <Image src={'https://cataas.com/cat'}/>
            <Description>
                {language.errors.unexpected.description}
            </Description>
        </Container>
    )

export default UnexpectedError