import styled from 'styled-components'

export const Container = styled.div`
    width:100%;
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const Title = styled.h1`
    color:  ${({ theme }) => theme.colors.red};
`

export const Description = styled.h2`
    color:  ${({ theme }) => theme.colors.gray_dark};
`