import React from 'react'
import {Strip} from './styled'
import Button from '../../atoms/button'
import PropTypes from 'prop-types'

const ButtonStrip = ({items, onClick, selected, disabled}) =>
    <Strip>
        {items.map((item, index) =>
            <Button
                key={index}
                selected={selected === item.value}
                light
                onClick={disabled ? null : () => onClick(item.value)}>
                {item.label}
            </Button>
        )}
    </Strip>

ButtonStrip.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
            label: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired
        })
    ).isRequired,
    onClick: PropTypes.func.isRequired,
    selected: PropTypes.string,
    disabled: PropTypes.bool
}

export default ButtonStrip
