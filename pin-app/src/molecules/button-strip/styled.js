import styled from 'styled-components'
import mediaQueries from '../../utils/media-queries'

export const Strip = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    ${mediaQueries.mobile`
      justify-content: space-around;
    `};
`