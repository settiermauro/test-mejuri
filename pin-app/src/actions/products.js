export const FETCH_PRODUCTS_PENDING = 'FETCH_PRODUCTS_PENDING'
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS'
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR'
export const LIKE_PRODUCT = 'LIKE_PRODUCT'
export const UNLIKE_PRODUCT = 'UNLIKE_PRODUCT'
export const ADD_PRODUCT_RENDER = 'ADD_PRODUCT_RENDER'

const fetchProductsPending = () => ({
    type: FETCH_PRODUCTS_PENDING
})

const fetchProductsSuccess = (products) => ({
    type: FETCH_PRODUCTS_SUCCESS,
    products: products
})

const fetchProductsError = (error) => ({
    type: FETCH_PRODUCTS_ERROR,
    error: error
})

export const fetchProducts = (type) =>
    dispatch => {
        dispatch(fetchProductsPending())
        fetch('http://dev-api.mejuri.com/api/v1/taxon/collections-by-categories/type' + (type ? `/${type}` : ''))
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw(res.error)
                }
                dispatch(fetchProductsSuccess(res))
                return res
            })
            .catch(error => {
                dispatch(fetchProductsError(error))
            })
    }

export const likeProduct = (product) =>
    dispatch => {
        dispatch({
            type: LIKE_PRODUCT,
            product
        })
    }

export const unlikeProduct = (product) =>
    dispatch => {
        dispatch({
            type: UNLIKE_PRODUCT,
            product
        })
    }

export const addProductRender = () =>
    dispatch => {
        dispatch({
            type: ADD_PRODUCT_RENDER
        })
    }