export const getProducts = state =>  state.products.products
export const getProductsPending = state => state.products.pending
export const getProductsError = state => state.products.error
export const getProductsLiked = state => state.products.likes
export const getProductsType = state => state.products.type_selected
export const getProductsRender = state => state.products.products_render