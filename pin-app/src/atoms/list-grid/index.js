import React from 'react'
import {Grid, Item} from './styled'
import PropTypes from 'prop-types'

const ListGrid = ({items}) =>
    <Grid>
        {items.map((item, index) =>
            <Item key={index}>
                {item}
            </Item>
        )}
    </Grid>

ListGrid.propTypes = {
    items: PropTypes.array.isRequired,
}

export default ListGrid
