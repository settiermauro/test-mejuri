import styled from 'styled-components'
import mediaQueries from '../../utils/media-queries'

export const Grid = styled.div`
    display: flex;
    flex-wrap: wrap
    width: 100%;
    justify-content: space-between;
    align-items: flex-start;
    ${mediaQueries.small`
      flex-direction: column;
      align-items: center;
    `};
`
export const Item = styled.div`
    margin-bottom: 40px;
    width: 250px;
    border-radius: 10px;
    background-color:  ${({ theme }) => theme.colors.gray_light};
    margin: 10px 20px;
    ${({ theme }) =>
    `
      box-shadow: 0 2px 10px 0 ${theme.shadow.black25}, 0 2px 5px 0 ${theme.shadow.black10};
    `};
    ${mediaQueries.mobile`
      width: 40%;
    `};
    ${mediaQueries.small`
      width: auto;
    `};
`