import React from 'react'
import Img from 'react-image'
import Loader from 'react-loader-spinner'
import theme from '../../theme'
import broken from '../../images/broken.png'
import { LoaderWrapper } from './styled'
import PropTypes from 'prop-types'

const Image = ({src}) => (
    <Img
        width={'100%'}
        src={src}
        unloader={<img width={'100%'} alt={'broken'} src={broken} />}
        loader={
            <LoaderWrapper>
                <Loader
                    type={'BallTriangle'}
                    color={theme.colors.blue} />
            </LoaderWrapper>
        } />
)

Image.propTypes = {
    src: PropTypes.string.isRequired,
}

export default Image
