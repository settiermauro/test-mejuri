import styled, { css } from 'styled-components'
import mediaQueries from '../../utils/media-queries'

export const StyledButton = styled.button`
    display: flex;
    align-items: center;
    color: ${({ theme }) => theme.colors.white};
    border-radius: 25px;
    border: none;
    padding: 5px 10px;
    cursor: pointer;
    background-color: ${({ theme }) => theme.colors.gray_dark};
    font-size: 15px;
    ${mediaQueries.mobile`
      padding: 3px 6px;
      font-size: 13px;
    `};
    ${({ theme }) =>
    `
      box-shadow: 0 2px 10px 0 ${theme.shadow.black25}, 0 2px 5px 0 ${theme.shadow.black10};
    `};
      
    ${({ big, theme }) =>
      big &&
      css`
        border-radius: 10px;
        background-color: ${theme.colors.red};
        padding: 10px 15px;
        font-size: 18px;
        ${mediaQueries.mobile`
          padding: 6px 12px;
          font-size: 15px;
        `};
    `};

    ${({ light, theme, selected }) =>
      light && 
      css`
        ${mediaQueries.mobile`
          padding: 2px 5px;
          font-size: 10px;
        `};
        padding: 2px 7px;
        border: 3px solid transparent;
        :hover {
            ${!selected &&
            css`
                border: solid 3px ${theme.colors.blue};
            `}
        }
        background-color: ${theme.colors.white};
        color: ${theme.colors.gray};
        font-weight: bold;
        ${selected && 
          css`
            background-color: ${theme.colors.gray_light};
            color: ${theme.colors.gray_dark};            
        `}
    `};
`

export const IconWrapper = styled.div`
    padding-right: 5px;
    justify-content: center;
`