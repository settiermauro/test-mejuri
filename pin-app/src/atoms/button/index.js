import React from 'react'
import { StyledButton, IconWrapper } from './styled'
import PropTypes from 'prop-types'


const Button = ({selected, light, big, onClick, icon, children}) => (
    <StyledButton selected={selected} light={light} big={big} onClick={onClick}>
        {icon && <IconWrapper>{icon}</IconWrapper>}
        {children}
    </StyledButton>
)

Button.propTypes = {
    selected: PropTypes.bool,
    light: PropTypes.bool,
    big: PropTypes.bool,
    onClick: PropTypes.func,
    icon: PropTypes.element
}

export default Button
