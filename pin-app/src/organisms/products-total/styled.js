import styled from 'styled-components'

export const Text = styled.span`
    color:  ${({ theme }) => theme.colors.gray};
    font-size: 16px;
    padding: 10px;
`

export const Big = styled.span`
    font-weight: bold;
    margin-right: 5px;
`