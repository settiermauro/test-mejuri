import React from 'react'
import {Big, Text} from './styled'
import language from '../../language'

const ProductsTotal = ({total}) =>
        <Text>
            <Big>
                {total.toLocaleString()}
            </Big>
            {language.labels.product_name}
        </Text>

export default ProductsTotal
