import styled from 'styled-components'

export const Container = styled.div`
    position: relative;
    width:100%;
`

export const Smoke = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right 0;
    background-color:  ${({ theme }) => theme.shadow.black25};
`

export const SaveContainer = styled.div`
    position: absolute;
    top:10px;
    right:10px;
`

export const MejuriContainer = styled.div`
    position: absolute;
    bottom:10px;
    left:10px;
`