import React, { Component } from 'react'
import {Container, MejuriContainer, SaveContainer, Smoke} from './styled'
import Image from '../../atoms/image'
import Button from '../../atoms/button'
import language from '../../language'
import { TiPin, TiPinOutline, TiLocationArrow } from 'react-icons/ti'

class ProductPreview extends Component {

    state = {
        show_details: false
    }

    render() {
        const { show_details } = this.state
        const {src, onSaveClick, onMejuriClick, isSaved} = this.props

        return (
            <Container
                onMouseOver={() => this.setState({show_details: true})}>
                <Image src={src}/>
                {show_details &&
                <Smoke
                    onMouseOut={() => this.setState({show_details: false})}>
                    <SaveContainer>
                        <Button
                            big
                            onClick={() => onSaveClick(isSaved)}
                            icon={isSaved ? <TiPin/> : <TiPinOutline/>}
                        >
                            {isSaved ? language.labels.remove : language.labels.save}
                        </Button>
                    </SaveContainer>
                    <MejuriContainer>
                        <Button
                            onClick={onMejuriClick}
                            icon={<TiLocationArrow/>}
                        >
                            {language.labels.mejuri}
                        </Button>
                    </MejuriContainer>
                </Smoke>
                }
            </Container>
        )
    }
}

export default ProductPreview
