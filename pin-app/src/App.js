import React from 'react'
import {applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {ThemeProvider} from 'styled-components'
import theme from './theme'
import rootReducer from './reducers'
import initial_state from './reducers/initial_state'
import Products from './containers/products'

const middlewares = [thunk]

const store = createStore(rootReducer, initial_state, applyMiddleware(...middlewares))


const App = () => {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <Products />
            </ThemeProvider>
        </Provider>
    )
}

export default App